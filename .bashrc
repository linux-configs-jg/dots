#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[[ -f ~/.welcome_screen ]] && . ~/.welcome_screen

_set_liveuser_PS1() {
    PS1='\u@\h \W\e[37m\]$(parse_git_branch)\$ '
    if [ "$(whoami)" = "liveuser" ] ; then
        local iso_version="$(grep ^VERSION= /usr/lib/endeavouros-release 2>/dev/null | cut -d '=' -f 2)"
        if [ -n "$iso_version" ] ; then
            local prefix="eos-"
            local iso_info="$prefix$iso_version"
            PS1="[\u@$iso_info \W]\$ "
        fi
    fi
}
_set_liveuser_PS1
unset -f _set_liveuser_PS1

ShowInstallerIsoInfo() {
    local file=/usr/lib/endeavouros-release
    if [ -r $file ] ; then
        cat $file
    else
        echo "Sorry, installer ISO info is not available." >&2
    fi
}


export PATH=$PATH:/home/jonathan/.local/bin
# When opening directories with neovim, cd into that directory first
# so NvimTree is in the right location and use the NvimTreeFocus toggle
# for an IDE like experience.
# When opening single files, keep it clean without the directory tree.
CleverOpenNeoVim() {
    ToOpen=$1
    if [ -d "${ToOpen}" ]; then
        cd "${ToOpen}" && nvim -c NvimTreeFocus .
    else
        nvim "${ToOpen}"
    fi
}

CreateDirectoryAndGoTo() {
    mkdir $1 && cd $1
}


alias ls='ls --color=auto'
alias ll='ls -lav --ignore=..'   # show long listing of all except ".."
alias l='ls -lav --ignore=.?*'   # show long listing but no hidden dotfiles except "."
alias vi='CleverOpenNeoVim'
alias pvpn='protonvpn-cli c -f' # connect to fastest protonvpn server
alias godir='CreateDirectoryAndGoTo'
alias hypr='vi ~/.config/hypr/hyprland.conf'
alias 'cmr'='docker compose -f ci/dev-compose.yml'

[[ "$(whoami)" = "root" ]] && return

[[ -z "$FUNCNEST" ]] && export FUNCNEST=100          # limits recursive functions, see 'man bash'

## Use the up and down arrow keys for finding a command in history
## (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

################################################################################
## Some generally useful functions.
## Consider uncommenting aliases below to start using these functions.
##
## October 2021: removed many obsolete functions. If you still need them, please look at
## https://github.com/EndeavourOS-archive/EndeavourOS-archiso/raw/master/airootfs/etc/skel/.bashrc

_open_files_for_editing() {
    # Open any given document file(s) for editing (or just viewing).
    # Note1:
    #    - Do not use for executable files!
    # Note2:
    #    - Uses 'mime' bindings, so you may need to use
    #      e.g. a file manager to make proper file bindings.

    if [ -x /usr/bin/exo-open ] ; then
        echo "exo-open $@" >&2
        setsid exo-open "$@" >& /dev/null
        return
    fi
    if [ -x /usr/bin/xdg-open ] ; then
        for file in "$@" ; do
            echo "xdg-open $file" >&2
            setsid xdg-open "$file" >& /dev/null
        done
        return
    fi

    echo "$FUNCNAME: package 'xdg-utils' or 'exo' is required." >&2
}

#------------------------------------------------------------

## Aliases for the functions above.
## Uncomment an alias if you want to use it.
##

# alias ef='_open_files_for_editing'     # 'ef' opens given file(s) for editing
# alias pacdiff=eos-pacdiff
################################################################################
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
# Colours
KONSOLE_DEFAULT='\[\e[39m\]'
RED='\e[31m\]'
GREEN='\e[32m\]'
YELLOW='\e[33m\]'
BLUE='\e[34m\]'
MAGENTA='\e[35m\]'
CYAN='\e[36m\]'
LIGHT_RED='\e[91m\]'
LIGHT_BLUE='\e[94m\]'
PINK='\e[95m\]'
LIGHT_GREEN='\[\e[38;5;157m\]'
GOLD='\[\e[38;5;222m\]'
PEACH='\[\e[38;5;216m\]'

## For light theme
#PS1=${KONSOLE_DEFAULT}'\u'${KONSOLE_DEFAULT}'@'${KONSOLE_DEFAULT}'\h'${GREEN}' \W'${LIGHT_RED}' $(parse_git_branch)'${KONSOLE_DEFAULT}' $ '

## For dark everforest theme use below
PS1=${GOLD}'\u'${KONSOLE_DEFAULT}'@'${GOLD}'\h'${LIGHT_GREEN}' \W'${BLUE}' $(parse_git_branch)'${KONSOLE_DEFAULT}' $ '
# PS1='\e[35m\]\u\e[37m\]@\e[35m\]\h \e[37m\]\W\e[37m\]$(parse_git_branch)\[\033[00m\] $ '
. "$HOME/.cargo/env"
